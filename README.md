# Less Tests

The following is a sandbox `ASP.NET MVC4` project in which to try out the `LESS` dynamic stylesheet language. Built with `C#`

*A reference to the documentation can be found [here][1]*

[1]: http://lesscss.org/